package com.bouali.spring.repositories;

import com.bouali.spring.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by BOUALI on 02/04/2018.
 */
public interface UserRepository extends JpaRepository<User, Long> {
}
