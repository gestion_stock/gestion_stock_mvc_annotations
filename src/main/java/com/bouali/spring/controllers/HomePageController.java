package com.bouali.spring.controllers;

import com.bouali.spring.entities.User;
import com.bouali.spring.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by BOUALI on 02/04/2018.
 */

@Controller
@RequestMapping("/home")
public class HomePageController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        List<User> users = userRepository.findAll();
        model.addAttribute("usersSize", users.size());
        return "index";
    }

    @RequestMapping(value = "/var", method = RequestMethod.GET)
    public String secondMethod(Model model) {
       model.addAttribute("localVar", "message");
        return "index";
    }
}
